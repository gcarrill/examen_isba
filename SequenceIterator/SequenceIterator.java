import java.util.*;

public class SequenceIterator {
    private PriorityQueue<SequenceEntry> pq;

    public SequenceIterator(Collection<Iterator<Comparable>> inputs) {
    	
    	if ( inputs == null) {
    		throw new NullPointerException("The 'inputs' Collection can't be null");
    	}
    	
        pq = new PriorityQueue<>(Comparator.comparing(SequenceEntry::getCurrentValue));
        
        // Initialize priority queue with the first element of each iterator
        for (Iterator<Comparable> iterator : inputs) {
            if (iterator.hasNext()) {
                pq.add(new SequenceEntry(iterator));
            }
        }
    }

    public boolean hasNext() {
        return !pq.isEmpty();
    }

    public Comparable next() {
        if (!hasNext()) {
            throw new NoSuchElementException("No more elements");
        }
        
        // Get the smallest element from the priority queue
        SequenceEntry entry = pq.poll();
        Comparable currentValue = entry.getCurrentValue();
        
        // Advance the iterator and add it back to the priority queue if it has more elements
        if (entry.hasNext()) {
            pq.add(entry);
        }
        
        return currentValue;
    }

    // Internal class to represent an entry in the priority queue
    private static class SequenceEntry {
        private Iterator<Comparable> iterator;

        public SequenceEntry(Iterator<Comparable> iterator) {
        	if ( iterator == null) {
        		throw new NullPointerException("The 'iterator' can't be null");
        	}
        	
        	this.iterator = iterator;
        }

        public Comparable getCurrentValue() {
            return iterator.next();
        }

        public boolean hasNext() {
            return iterator.hasNext();
        }
    }
}