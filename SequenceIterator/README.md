## Iterador sobre múltiples secuencias

Crear una clase SequenceIterator que permita obtener de forma ordenada los elementos de un conjunto de secuencias de entrada iterables:  
Ejemplo:  
Entrada: { [1,3,5,7,9,....] , [2,4,6,8,...], [0,10,20,30,...], ....}  
Salida: Un iterador sobre la secuencia 0,1,2,3,4,5,6,7,8,9,10...  
Las secuencias de entrada se encuentran ordenadas y las mismas pueden ser arbitrariamente grandes (no es posible cargarlas enteras en memoria).  
La clase debe contener estos métodos, (pueden agregarse los que hagan falta):  

```
public class SequenceIterator {
    
    public SequenceIterator(Collection<Iterator<Comparable>> inputs) {
        ...
    }
    
    public boolean hasNext() {
        ...
    }
    
    public Comparable next() {
        ...
    }
}
```

