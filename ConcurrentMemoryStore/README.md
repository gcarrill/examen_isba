## ConcurrentMemoryStore

Se tiene la clase  
```
class Item {
    private int value1;
    private int value2;
    
    public void setValue1(int v) {
        value1=v;
    }

    public void setValue2(int v) {
        value2=v;
    }

    public int getValue1() {
        return value1;
    }

    public int getValue2() {
        return value2;
    }
}
```

Implementar la interfaz:  

```
public interface ConcurrentMemoryStore {
    
    /**
    * Almacena un Item asociado a una clave key
    * @throws IllegalArgumentException Si ya existe un valor
    * asociado a la clave
    */
    void store(String key, Item item) throws IllegalArgumentException;
    
    /**
    * Actualiza los valores del Item asociado a key.
    * La instancia de Item que queda almacenada no debe cambiar.
    **/
    void update(String key, int value1, int value2);
    
    /**
    * Retorna un iterador sobre los Items contenidos
    */
    Iterator<Item> valueIterator();
    
    /**
    * Borra el Item con clave key
    */
    void remove(String key);
}
```

La implementación debe ser thread-safe y lo más eficiente posible. El iterador retornado por `valueIterator()` debe ser recorrible sin riesgo de una `ConcurrentModificationException`.

