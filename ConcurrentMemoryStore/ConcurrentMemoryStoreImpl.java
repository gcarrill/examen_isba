import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentMemoryStoreImpl implements ConcurrentMemoryStore {
    private final Map<String, Item> storeMap = new ConcurrentHashMap<>();
    private final Lock lock = new ReentrantLock();

    @Override
    public void store(String key, Item item) throws IllegalArgumentException {
        if (storeMap.containsKey(key)) {
            throw new IllegalArgumentException("Ya existe un valor asociado a la clave");
        }
        storeMap.put(key, item);
    }

    @Override
    public void update(String key, int value1, int value2) {
        lock.lock();
        try {
            Item existingItem = storeMap.get(key);
            if (existingItem != null) {
                existingItem.setValue1(value1);
                existingItem.setValue2(value2);
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Iterator<Item> valueIterator() {
        return storeMap.values().iterator();
    }

    @Override
    public void remove(String key) {
        storeMap.remove(key);
    }
}

class Item {
	private int value1;
	private int value2;

	public void setValue1(int v) {
		value1=v;
	}
	
	public void setValue2(int v) {
		value2=v;
	}
	
	public int getValue1() {
		return value1;
	}
	
	public int getValue2() {
		return value2;
	}
}