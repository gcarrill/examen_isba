# Examen Isba

## JAVA

### 1 - Dé un ejemplo de una situación que requiera el uso de ThreadLocal
Un ejemplo donde usar ThreadLocal seria una aplicacion web donde cada request es manejado por un thread diferente y es necesario almacenar el contexto de cada request para cada thread. 

#### 2 - ¿Cuándo es necesario marcar una variable como volatile?
Marcar una variable como volatile es necesario cuando se necesita garantizar la visibilidad de las modificaciones de esta entre multiples threads.

#### 3 - ¿Qué lleva a una situación de deadlock? ¿Cómo puede resolverse?
Un deadlock se produce cuando 2 o mas procesos o hilos se bloquean indefinidamente esperando que se libere un recurso retenido por otro proceso o hilo bloqueado por la misma situacion. Dependiendo de la situacion podria finalizarse uno de los procesos o hilos para que libere los recursos retenidos y se salga de la situacion de deadlock (con una posible perdida de informacion). Otra opcion posible es usar un timeout en la adquicicion de un recurso, y si este se alcansa el proceso u hilo libera los recursos que posee y vuelve a intentar mas tarde adquirirlos.

## SQL

Sean los siguientes esquemas relacionales:  
Vuelo(VueloNro, Desde, Hacia, Distancia, Partida, Arribo, Precio)  
Aeronave(AId, ANombre, Rango)  
Certificado(EId, AId)  
Empleado(EId, Enombre, Sueldo)  
La relación Empleado contiene datos de todos los empleados de la compañía, entre
ellos los pilotos.  
En la relación Certificado solo figuran los pilotos certificados para volar una
determinada aeronave.  
Responder la siguiente consulta en SQL:  
“Listar los nombres de los pilotos que pueden volar aeronaves con rango de crucero
mayor a 5000 millas pero que solo está certificado con aviones Boeing”


```
SELECT Empleado.Enombre
FROM Empleado
INNER JOIN Certificado ON Empleado.EId = Certificado.EId
INNER JOIN Aeronave ON Certificado.AId = Aeronave.AId
WHERE Aeronave.Rango > 5000
AND Aeronave.ANombre = 'Boeing'
AND NOT EXISTS (
    SELECT 1
    FROM Certificado C
    WHERE C.EId = Empleado.EId
    AND NOT EXISTS (
        SELECT 1
        FROM Aeronave A
        WHERE C.AId = A.AId
        AND A.ANombre = 'Boeing'
    )
);
```

